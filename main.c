#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"
#include "time.h"


#define M 15
#define K 15
#define N 15



int A [M][K];				//Matrix A
int B [K][N];				//Matrix B
int C [M][N];				//Matrix A*B


#define NUM_THREADS 2

struct v 
{
   int i; /* row */	
   int j; /* column */
};



int main(int argc, char *argv[]) {

   int i,j, count = 0;
   srand (time(NULL));
   clock_t begin,end;
   float time;
   begin=clock();

   for(i=0; i < M; i++){			//add values for matrix A
	for(j=0; j < K; j++){
		A[i][j]=rand() % 10;

	}
    }

   for(i=0; i < K; i++){			//add values for matrix B
	for(j=0; j < N; j++){
		B[i][j]=rand() % 10;
	}
    }
	     

   for(i = 1; i <= NUM_THREADS; i++) {
      
         //Assign a row and column for each thread
         struct v *__restrict__ data;data = (struct v *) malloc(sizeof(struct v));
         data->i = M/i;		//2
         data->j = K/i;		//2

         /* Thread passing it data as a parameter */
         pthread_t tid;       //Thread ID
         pthread_attr_t attr; //Set of thread attributes

        
         pthread_attr_init(&attr);
        
	 //Create the thread
         if(pthread_create(&tid,&attr,runner,data))
		printf("Error creating thread\n");

         //parent waits for all thread to complete
         pthread_join(tid, NULL);
         count++;
   }
	end=clock();
 	time= (float)(end - begin) / CLOCKS_PER_SEC;
	printf("\nSuccess\n");
  	printf("\nTime Taken: %f",time,"\n\n");

}
