all: matmult

matmult: main.o matrix.o
	gcc -Wall main.o matrix.o -o matmult -lpthread

main.o: main.c
	gcc -c -Wall main.c -lpthread


matrix.o: matrix.c
	gcc -c -Wall matrix.c

clean:
	rm -rf *.o matmult
