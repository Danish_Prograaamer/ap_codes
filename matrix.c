#include "matrix.h"
#define NUM_THREADS 2

#define M 15
#define K 15
#define N 15

struct v 
{
   int i; /* row */	
   int j; /* column */
};

void *runner(void *param) {

   struct v *data = param; 					
int A [M][K];				
int B [K][N];
int C [M][N];
				
   int n, sum = 0; 
   int m,p;
   int counter=data->i;		
   int limit=counter-(counter / NUM_THREADS);			//the counter 
   

for (m= counter; m >= limit; m--){  
   for(n = 1; n <= N; n++){
       	for(p = 1; p <= K; p++)
       {
	   sum += A[m-1][p-1] * B[p-1][n-1];
       }
       C[m-1][n-1] = sum;
       sum=0;
    }
}
  						 	
   pthread_exit(0);
}
