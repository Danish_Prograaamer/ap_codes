#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


#define M 1504
#define K 1504
#define N 1504

#define NUM_THREADS 32	//number of threads being used

int A [M][K];				//Matrix A
int B [K][N];				//Matrix B
int C [M][N];				//Matrix A*B




struct v 
{
   int i; /* row */	
   int j; /* column */
};

void *runner(void *param); /* the thread */

int main(int argc, char *argv[]) {

   int i,j, count = 0;
   srand (time(NULL));

   for(i=0; i < M; i++){			//add values for matrix A
	for(j=0; j < K; j++){
		A[i][j]=rand() % 10;

	}
    }

   for(i=0; i < K; i++){			//add values for matrix B
	for(j=0; j < N; j++){
		B[i][j]=rand() % 10;
	}
    }
	     

   for(i = 1; i <= NUM_THREADS; i++) {
      
         //Assign a row and column for each thread
         struct v *__restrict__ data;data = (struct v *) malloc(sizeof(struct v));
         data->i = M/i;		//2
         data->j = K/i;		//2

         /* Thread passing it data as a parameter */
         pthread_t tid;       //Thread ID
         pthread_attr_t attr; //Set of thread attributes

        
         pthread_attr_init(&attr);
        
	 //Create the thread
         if(pthread_create(&tid,&attr,runner,data))
		printf("Error creating thread\n");

         //parent waits for all thread to complete
         pthread_join(tid, NULL);
         count++;
    }
/*
for(i = 0; i < M; i++) {
	for(j = 0; j < K; j++) {
         	printf("%d ", A[i][j]);
      }
      printf("\n");
}

printf("\n\n");
for(i = 0; i < K; i++) {
	for(j = 0; j < N; j++) {
         	printf("%d ", B[i][j]);
      }
      printf("\n");
}

   
printf("\n\n");
   printf("\n%d threads used\n",count);
   //Print out the resulting matrix
for(i = 0; i < M; i++) {
	for(j = 0; j < N; j++) {
         	printf("%d ", C[i][j]);
      }
      printf("\n");
}
*/
}

									//The thread will begin control in this function
void *runner(void *param) {
   struct v *data = param; 						// the structure that holds our data
   int n, sum = 0; 
   int m,p;
   int counter=data->i;	
   int limit=counter-(counter / NUM_THREADS);			//the counter 
   

for (m= counter; m >= limit; m--){  
   for(n = 1; n <= N; n++){
       	for(p = 1; p <= K; p++)
       {
	   sum += A[m-1][p-1] * B[p-1][n-1];
       }
       C[m-1][n-1] = sum;
       sum=0;
    }
}
  						 	
   pthread_exit(0);
}
